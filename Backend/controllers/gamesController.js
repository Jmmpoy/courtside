// gamesControllers.js
const Game = require('../models/games');

// Defining all methods and business logic for routes

module.exports = {
	findAll: function(req, res) {
		Game.find(req.query)
			.then(games => res.json(games))
			.catch(err => res.status(422).json(err));
			
	},
	findById: function(req, res) {
		Game.findById(req.params.id)
			.then(game => res.json(game))
			.catch(err => res.status(422).json(err));
	},
	create: function(req, res) {
		Game.create(req.body)
			.then(newGame => res.json(newGame))
			.catch(err => res.status(422).json(err));
	},
	update: function(req, res) {
		Game.findOneAndUpdate({ _id: req.params.id }, req.body)
			.then(game => res.json(game))
			.catch(err => res.status(422).json(err));
	},
	remove: function(req, res) {
		Game.findById({ _id: req.params.id })
			.then(game => game.deleteOne())
			.then(allgames => res.json(allgames))
			.catch(err => res.status(422).json(err));
	}
};