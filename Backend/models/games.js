const  mongoose = require('mongoose');
const Schema = mongoose.Schema;

const gameSchema = new Schema ({
    date : {
        type: Date,
        required: true    
    },
    lieu : {
        type: String,
        required: true    
    },
    heure:{
        type: Number,
        required: true
    },
    niveau:{
        type: String,
        enum:['confirmé','amateur','intermédiaire'],
        required: true
    },
    infrastructure:{
        type: String,
        enum:['intérieur','extérieur'],
        required: true
    },
    durée:{
        type: Number,
        required: false
    },
    nombreDejoueurs:{
        type: Number,
        required: false
    }
})

const Game = mongoose.model('Game', gameSchema);

module.exports = Game;