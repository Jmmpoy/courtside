// users.js

const router = require('express').Router();
const usersController = require('../controllers/usersController');


//I have separated the concerned routes that match a specific URL. 
//For example, routes that are starting with :id routing parameter 
//are defined above together in the file. 


// router.post('/register/', usersController.register);
// router.post('/login/', usersController.login);

module.exports = router;

