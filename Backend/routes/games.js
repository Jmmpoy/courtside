// games.js

const router = require('express').Router();
const gamesController = require('../controllers/gamesController');



//I have separated the concerned routes that match a specific URL. 
//For example, routes that are starting with :id routing parameter 
//are defined above together in the file. 


router
	.route('/')
	.get(gamesController.findAll)
	.post(gamesController.create);

router
	.route('/:id')
	.get(gamesController.findById)
	.put(gamesController.update)
	.delete(gamesController.remove);


module.exports = router;