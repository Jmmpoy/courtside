import React, { Component } from 'react';
import { Container } from './MapStyle';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import Sidebar from './Sidebar';
import './styles.css';


class Maps extends Component {
    render() {

        const GoogleMapExample = withGoogleMap(props => (
            <GoogleMap
                defaultCenter={{ lat: 48.092, lng: -1.685 }}
                defaultZoom={13}
                
            >
            {props.isMarkerShown && <Marker position={{ lat: -34.397, lng: 150.644 }} />}
            </GoogleMap>
            
        ));

        return (
            <Container>
                    <Sidebar/>
                    <div id="page-wrap">
                        <div className="row">
                            <div className="col-2 mt-1">
                            {/* <button><i className="fas fa-bars"></i></button> */}
                            </div>
                            <div className="col-8 d-flex justify-content-center"><input type="text" className="form-control form-control-sm mb-1" placeholder="Recherche" aria-label="Username" aria-describedby="basic-addon1"/></div>
                            <div className="col-2 mt-2 d-flex justify-content-end flex-nowrap"><i className="fas fa-clipboard-list"></i></div>
                        </div>
                        <div className="row">
                            <div className="col-12  d-flex justify-content-center" id="mapid">
                                <GoogleMapExample
                                    containerElement={<div style={{ height: `630px`, width: '630px', position:'relative'}} />}
                                    mapElement={<div style={{ height: `100%` }} />}
                                /> 
                            </div>
                        </div>
                    </div>
            </Container>


        );
    }
}



export default Maps;



