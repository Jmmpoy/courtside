import styled from 'styled-components';

export const Container = styled.div`
height: 100%;
width: 100%;

`

export const Search = styled.div`
     padding: 5px;
     font-size: 15px;
     border-width: 0px;
     border-color: #CCCCCC;
     background-color: #FFFFFF;
     color: #000000;
     border-style: hidden;
     border-radius: 18px;
     box-shadow: 0px 0px 5px rgba(66,66,66,.75);
     text-shadow: 0px 0px 5px rgba(66,66,66,.75);
     outline:none;
`




         
