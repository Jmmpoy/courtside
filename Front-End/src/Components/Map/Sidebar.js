import React from 'react';
import { slide as Menu } from "react-burger-menu";

export default props => {
  return (
    <Menu>

        <a className="menu-item--small d-flex justify-content-end" href="matchs">
          Matchs
        </a>
      

      <a className="menu-item d-flex justify-content-end" href="/mesmatchs">
        Mes Matchs
      </a>

      <a className="menu-item d-flex justify-content-end " href="/notifiactions">
        Notifications
      </a>

      <a className="menu-item d-flex justify-content-end" href="/profil ">
        Mon Profil
      </a>
    </Menu>
  );
};