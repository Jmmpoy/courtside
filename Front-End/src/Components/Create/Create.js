import React, { Component } from "react";
import { Opacity, Button } from "./CreateStyle";
import axios from "axios";
import FadeIn from "react-fade-in";
import "./style.css";

class Create extends Component {
  constructor() {
    super();
    this.state = {
      durée: "",
      date: "",
      lieu: "",
      heure: "",
      niveau: "",
      infrastructure: ""
    };
  }

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = event => {
    event.preventDefault();
    // get our form data out of state
    const { date, durée, lieu, infrastructure, heure, niveau } = this.state;
    console.log();
    axios
      .post("http://127.0.0.1:5000/api/games", {
        date,
        lieu,
        heure,
        niveau,
        durée,
        infrastructure
      })
      .then(res => {
        console.log(res);
        console.log(res.data);
      });
     window.location.pathname = "/Matchs";
  };

  render() {
    return (
      <FadeIn>
        <div className="container">
          <div className="row pt-4 ">
            <div className="col-12 mx-auto">
              <p className="title text-center">Nouveau Match</p>
            </div>
          </div>

          <Opacity>
            <form className="form text-center mx-auto" onSubmit={this.onSubmit}>
              <div className="col-12 text-center mx-auto">
                <div className="div">
                  Date:
                  <br />
                  <input
                    style={{ border: "none" }}
                    required="required"
                    type="Date"
                    name="date"
                    value={this.state.date}
                    onChange={this.onChange}
                  />
                  <br />
                </div>

                <div className="div">
                  Lieu:
                  <br />
                  <input
                    style={{ borderStyle: "solid" }}
                    required="required"
                    placeholder="Rentrez un lieu"
                    type="text"
                    name="lieu"
                    value={this.state.lieu}
                    onChange={this.onChange}
                  />
                  <br />
                </div>
              </div>

              <div className="div mx-auto">
                Infrastructure:
                <br />
                <select
                  name="infrastructure"
                  value={this.state.infrastructure}
                  onChange={this.onChange}
                >
                  <option value="intérieur">Intérieur</option>
                  <option value="extérieur">Extérieur</option>
                </select>
              </div>

              <div className="div mx-auto">
                Heure:
                <br />
                <input
                  style={{ borderStyle: "solid" }}
                  placeholder="Rentrez une Horaire"
                  type="text"
                  name="heure"
                  value={this.state.heure}
                  onChange={this.onChange}
                />
              </div>

              <div className="div mx-auto">
                Durée:
                <br />
                <input
                  style={{ borderStyle: "solid" }}
                  placeholder="Rentrez une durée"
                  type="text"
                  name="durée"
                  value={this.state.durée}
                  onChange={this.onChange}
                />
              </div>

              <div className="div mx-auto">
                Niveau:
                <br />
                <select
                  name="niveau"
                  value={this.state.niveau}
                  onChange={this.onChange}
                >
                  <option value="amateur">Amateur</option>
                  <option value="intermédiaire">Intermédiaire</option>
                  <option value="confirmé">Confirmé</option>
                </select>
              </div>

              <button
                style={{ border: "none" }}
                className="mx-auto btnsubmit btn btn-warning"
                type="submit"
                value="Submit"
                onClick={this.onSubmit}
              >
                Valider les Informations
              </button>
            </form>
          </Opacity>
          <div className="row">
            <div className="col-2"></div>
            <div className="col-8 text-center"></div>
            <div className="col-2"></div>
          </div>
        </div>
      </FadeIn>
    );
  }
}

export default Create;
