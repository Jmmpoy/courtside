import React, { Component } from "react";
import { Link } from "react-router-dom";
import FadeIn from 'react-fade-in';
import './style.css'

class CreateProfil extends Component {
  render() {
    return (
      <FadeIn delay={350}>
      <div className="container-fluid">
        <div className="row">
          <div className="col-12">
            <h1 className="text-center mb-5">Profil</h1>
            <form className="text-center create">
              <input className="mb-3" type="text" placeholder="Email:" />
              <br/>
              <input className="mb-3" type="text" placeholder="Mot de passe" />
              <br/>
              <input className="mb-3" type="text" placeholder="Confirmez mot de passe "/>
              <br/>
              <Link to="./Matchs">
              <button className="button">Valider</button>
              </Link>
            </form>
          </div>
        </div>
      </div>
      </FadeIn>
    );
  }
}

export default CreateProfil;
