import React, { Component } from "react";

class BlockJaune extends Component {
  constructor(props) {
    super(props);
    this.state = {
      games: []
    };
  }

  

  render() {
  const game = this.props.game
console.log(game)
    return (
      <div className="col-9 mx-auto block pt-5 mb-5">
        <div className="row ">
          <div className="col-3 text-left username"></div>
          <div className="col-6"></div>
          <div className="col-3 places text-right"></div>
          <div className="col-12 text-left infos mx-auto mb-2">
            <p>Date:{game.date}</p>
            <p>Lieu: {game.lieu}</p>
            <p>Heure: {game.heure}H</p>
            <p>Durée: {game.durée}H</p>
            <p>Niveau: {game.niveau}</p>
            <p>Infrastructure: {game.infrastructure}</p>
            <hr></hr>
          </div>
          <div className="col-9 photos text-left"></div>
          <div className="col-3 btnplus">
            <i className="fas fa-plus"></i>
          </div>
          <div className="col-12 participants"></div>
        </div>
        <div className="loader">
          <div className="icon"></div>
        </div>
      </div>
    );
  }
}

export default BlockJaune;
