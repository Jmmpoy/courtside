import React, { Component } from 'react';
import './Matchs.css';
import { Link } from 'react-router-dom';
import BlockJaune from './BlockJaune';
import axios from 'axios';


class Matchs extends Component {
  constructor(props){
  super(props);
    this.state={
      games : []
    }
  }

  componentDidMount() {
    axios.get('http://127.0.0.1:5000/api/games/')
      .then(response => {
          this.setState({
            games: response.data
          });

      })
      .catch(error => {
        console.log(error);
      })
 }

  render() {
    const Games = this.state.games.map((game,index)=>
      <BlockJaune key={index} game={game}></BlockJaune>
    );
    return (
      
          <div className="container">
            <div className="row pt-5">
              <div className="col-12 text-right title">Matchs</div>
            </div>
            <div className="row">
              <div className="col-12 btnblock text-center">
                <Link to="./Create"> 
                  <button className="btn btn-warning" >Créer un match</button>
              </Link>
              </div>
              <div className="col-12 text-center mt-2 games">
                {Games}
              </div>
            </div>
          </div>
        
      
    );
  }
}

export default Matchs;