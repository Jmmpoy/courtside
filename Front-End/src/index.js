import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Matchs from './Components/Matchs/Matchs';
import Map from './Components/Map/Map';
import Create from './Components/Create/Create';
import CreateProfil from './Components/CreateProfil/CreateProfil';
import 'bootstrap/dist/css/bootstrap.css';
import { Route, BrowserRouter as Router} from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

const routing = (
        <Router history={Router}>
          <div>
            <Route exact path="/" component={App} />
            <Route path="/CreateProfil" component={CreateProfil} />
            <Route path="/Map" component={Map} />
            <Route path="/Create" component={Create} />
            <Route path="/Matchs" component={Matchs} /> 
          </div>
        </Router>
  )

ReactDOM.render(routing, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
