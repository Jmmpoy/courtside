import React, { Component } from "react";
import "./App.css";
import { Link } from "react-router-dom";
import FadeIn from "react-fade-in";
import axios from 'axios'
class App extends Component {
  state = {
    username:'',
    password:''
  };


  handleChange = (e)=>{
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = (e) =>{
    e.preventDefault();
    const {username,password}= this.state;
    axios
      .post("http://127.0.0.1:5000/api/auth/login", {
        username,
        password
      })
      .then(res => {
        console.log(res)
        if(res.data){
          console.log('successful login')
          this.setState({
            redirectTo:'/Matchs'
          })
        }else{
          console.log('Sign-up error')
        }
        
      }).catch(error =>{
        console.log('Sign up server error:')
        console.log(error)
      });
      window.location.pathname = '/Matchs'
  }
  render() {


    return (
      <FadeIn delay={350}>
        <div className="container">
          <h1 className="title">Courtside</h1>
          <div className="row padding">
            <div className="col-12 connexion text-center">
              <form onSubmit={this.handleSubmit}>
                <input
                onChange={this.handleChange}
                  name="username"
                  placeholder="Username"
                  className="button2 mb-2"
                />
                <input 
                onChange={this.handleChange}
                name="password" 
                placeholder= "Password" 
                className="button2 mb-2" />
                <br />
                <Link to="./Matchs">
                  <button className="button">Connexion</button>
                </Link>
              </form>
              <Link to="./CreateProfil">
                <p className="pt-4 new">Créer un Compte </p>
              </Link>
            </div>
          </div>
        </div>
      </FadeIn>
    );
  }
}

export default App;
